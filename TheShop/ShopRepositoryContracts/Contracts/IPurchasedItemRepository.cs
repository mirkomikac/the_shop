﻿using Data.Models;

namespace TheShop.RepositoryContracts.Contracts
{
    public interface IPurchasedItemRepository
    {
        PurchasedItem GetById(int id);
        void Save(PurchasedItem item);
    }
}
