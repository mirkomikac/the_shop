﻿using System;

namespace TheShop.ServiceContracts.Dto
{
    public class OrderDto
    {
        public Guid ItemId { get; set; } 
        public int BuyerId { get; set; }
        public int MaxExpectedPrice { get; set; }
    }
}
