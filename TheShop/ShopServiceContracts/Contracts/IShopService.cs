﻿using Data.Models;
using TheShop.ServiceContracts.Dto;

namespace TheShop.ServiceContracts.Contracts
{
    public interface IShopService
    {
        PurchasedItem GetById(int id);
        void OrderAndSellItem(OrderDto orderDto);
    }
}
