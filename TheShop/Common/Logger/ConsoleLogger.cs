﻿using System;

namespace TheShop.Common.Logger
{
    public class ConsoleLogger : ILogger
    {
        private static ConsoleLogger Instance { get; set; }

        private ConsoleLogger()
        {
        }
        public static ConsoleLogger GetInstance()
        {
            if(Instance == null)
            {
                Instance = new ConsoleLogger();
            }

            return Instance;
        }

        public void Info(string message)
        {
            Console.WriteLine($"{DateTime.Now} Info: {message}");
        }

        public void Error(string message)
        {
            Console.WriteLine($"{DateTime.Now} Error: {message}");
        }

        public void Debug(string message)
        {
            Console.WriteLine($"{DateTime.Now} Debug: {message}");
        }
    }
}
