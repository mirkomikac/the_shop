﻿using System;
using TheShop.SupplierContracts.SupplierContracts;

namespace TheShop.SupplierContracts.Contracts
{
    public interface ISupplierService
    {
        ItemDto GetItem(Guid guid);
        bool ItemInInventory(Guid guid);
    }
}
