﻿using System;
using System.Collections.Generic;
using Data.Models;
using TheShop.InMemoryRepository;

namespace TheShop.RepositoryUnitTests
{
    public class PurchasedItemRepositoryFixture
    {
        public PurchasedItemRepository PurchasedItemRepository { get; set; }

        public PurchasedItemRepositoryFixture()
        {
            var initialData = new List<PurchasedItem>() { };
            PurchasedItemRepository = new PurchasedItemRepository(initialData);

            PurchasedItemRepository.Save(new PurchasedItem { Id = 1, ItemGuid = Guid.NewGuid(), Price = 100, PurchaseDate = DateTime.Now, BuyerUserId = 1 });
            PurchasedItemRepository.Save(new PurchasedItem { Id = 2, ItemGuid = Guid.NewGuid(), Price = 100, PurchaseDate = DateTime.Now, BuyerUserId = 2 });
            PurchasedItemRepository.Save(new PurchasedItem { Id = 3, ItemGuid = Guid.NewGuid(), Price = 100, PurchaseDate = DateTime.Now, BuyerUserId = 3 });
        }
    }
}
