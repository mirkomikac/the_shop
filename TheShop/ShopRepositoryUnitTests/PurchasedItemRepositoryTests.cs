using Data.Models;
using System;
using TheShop.InMemoryRepository;
using Xunit;

namespace TheShop.RepositoryUnitTests
{
    public class PurchasedItemRepositoryTests : IClassFixture<PurchasedItemRepositoryFixture>
    {
        private PurchasedItemRepositoryFixture _purchasedItemRepositoryFixture;
        private PurchasedItemRepository _purchasedItemRepository;

        public PurchasedItemRepositoryTests(PurchasedItemRepositoryFixture purchasedItemRepositoryFixture)
        {
            _purchasedItemRepositoryFixture = purchasedItemRepositoryFixture;
            _purchasedItemRepository = _purchasedItemRepositoryFixture.PurchasedItemRepository;
        }

        [Fact(DisplayName = "Retrieve a purchased item successfully")]
        public void TestRetrieveAPurchasedItemSuccessfully()
        {
            var purchasedItemId = 1;
            Assert.NotNull(_purchasedItemRepository.GetById(purchasedItemId));
        }

        [Fact(DisplayName = "Try retrieving a non existant item")]
        public void TestTryRetrievingANonExistantItem()
        {
            int invalidId = -1;
            Assert.ThrowsAny<Exception>(() => _purchasedItemRepository.GetById(invalidId));
        }

        [Fact(DisplayName = "Save a purchased item successfully")]
        public void TestSaveAPurchasedItemSuccessfully()
        {
            var guid = Guid.NewGuid();
            var purchasedItem = new PurchasedItem { ItemGuid = guid, Price = 100, PurchaseDate = DateTime.Now, BuyerUserId = 1 };

            _purchasedItemRepository.Save(purchasedItem);

            var expectedId = 4;
            var purchasedItemRetrieved = _purchasedItemRepository.GetById(expectedId);
            
            Assert.NotNull(purchasedItem);
            Assert.Equal(guid, purchasedItemRetrieved.ItemGuid);
        }

        [Fact(DisplayName = "Try saving a purchased item with missing properties")]
        public void TestTrySavingAPurchasedItemWithMissingProperties()
        {
            var purchasedItem = new PurchasedItem { ItemGuid = Guid.NewGuid(), BuyerUserId = 1 };

            Assert.Throws<ArgumentOutOfRangeException>(() => _purchasedItemRepository.Save(purchasedItem));
        }

        [Fact(DisplayName = "Try saving a null valued purchased item")]
        public void TestTrySavingANullValuedPurchasedItem()
        {
            Assert.Throws<ArgumentNullException>(() => _purchasedItemRepository.Save(null));
        }
    }
}
