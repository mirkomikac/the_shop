﻿using System.Collections.Generic;
using TheShop.Common.Logger;
using TheShop.InMemoryRepository;
using TheShop.RepositoryContracts.Contracts;
using TheShop.SupplierContracts.Contracts;
using TheShop.SupplierServices;
using TheShop.ShopServices.Services;
using TheShop.ServiceContracts.Contracts;
using Data.Models;

namespace TheShop.BasicShopServiceFactory
{
    public class BasicShopFactory : IFactoryMethod<IShopService>
    {

        public IShopService Create()
        {
            ILogger logger = ConsoleLogger.GetInstance();
            ISupplierService aaSupplier = new AASupplierService();
            ISupplierService deltaSupplier = new DeltaSupplierService();
            ISupplierService fastSupplierservice = new FastSupplierService();
            
            IList<PurchasedItem> initalItems = new List<PurchasedItem>();
            IPurchasedItemRepository itemRepository = new PurchasedItemRepository(initalItems);

            ICollection<ISupplierService> suppliers = new List<ISupplierService>() { aaSupplier, deltaSupplier, fastSupplierservice };

            var shopService = new ShopService(logger, itemRepository, suppliers);
            return shopService;
        }
    }
}
