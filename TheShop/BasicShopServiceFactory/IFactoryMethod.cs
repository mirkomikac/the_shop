﻿namespace TheShop.BasicShopServiceFactory
{
    public interface IFactoryMethod<T>
    {
        T Create();
    }
}
