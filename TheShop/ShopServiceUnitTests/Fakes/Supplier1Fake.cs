﻿using System;
using TheShop.SupplierContracts.Contracts;
using TheShop.SupplierContracts.SupplierContracts;

namespace TheShop.ServiceUnitTests.Fakes
{
    public class Supplier1Fake : ISupplierService
    {
        public bool ItemInInventory(Guid guid)
        {
            if (guid == Guid.Parse("9cd9c036-db33-41a8-8b44-f6401b6aded9"))
            {
                return true;
            }

            return false;
        }

        public ItemDto GetItem(Guid guid)
        {
            if (guid == Guid.Parse("9cd9c036-db33-41a8-8b44-f6401b6aded9"))
            {
                return new ItemDto()
                {
                    Id = Guid.Parse("9cd9c036-db33-41a8-8b44-f6401b6aded9"),
                    Name = "ItemDto from AA supplier",
                    Price = 458
                };
            }

            return null;
        }
    }
}
