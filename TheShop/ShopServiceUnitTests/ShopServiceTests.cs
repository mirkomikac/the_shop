using Data.Models;
using Moq;
using System;
using TheShop.ServiceContracts.Contracts;
using TheShop.ServiceContracts.Dto;
using Xunit;

namespace TheShop.ServiceUnitTests
{
    public class ShopServiceTests : IClassFixture<ShopServiceFixture>
    {
        private ShopServiceFixture _shopServiceFixture;
        private IShopService _shopService;

        public ShopServiceTests(ShopServiceFixture shopServiceFixture)
        {
            _shopServiceFixture = shopServiceFixture;
            _shopService = shopServiceFixture.ShopService;
        }

        [Fact(DisplayName = "Make an order successfully")]
        public void TestMakingAnOrderSuccessfully()
        {
            var orderDto = new OrderDto() { ItemId = Guid.Parse("9cd9c036-db33-41a8-8b44-f6401b6aded9"), BuyerId = 20, MaxExpectedPrice = 500 };
            _shopServiceFixture.PurchasedItemRepositoryMock.Setup(x => x.Save(It.IsAny<PurchasedItem>())).Verifiable();

            _shopService.OrderAndSellItem(orderDto);

            _shopServiceFixture.PurchasedItemRepositoryMock.Verify(x => x.Save(It.IsAny<PurchasedItem>()), Times.Once());
        }        

        [Fact(DisplayName = "Try to make an order with null value")]
        public void TryToMakeAnOrderWithNullValue()
        {
            Assert.Throws<ArgumentNullException>(() => _shopService.OrderAndSellItem(null));
        }

        [Fact(DisplayName = "Try to make an order with an unknown item Id")]
        public void TestTryToMakeAnOrderWithAnUnknownItemId()
        {
            var itemId = Guid.Empty;
            OrderDto orderDto = new OrderDto { ItemId = itemId, BuyerId = 0, MaxExpectedPrice = 500 };
            Assert.Throws<Exception>(() => _shopService.OrderAndSellItem(orderDto));
        }

        [Fact(DisplayName = "Try to make an order with no matching pricing range")]
        public void TestTryToMakeAnOrderWithNoMatchingPricingRange()
        {
            var itemId = Guid.Empty;
            OrderDto orderDto = new OrderDto { ItemId = itemId, BuyerId = 0, MaxExpectedPrice = 10 };
            Assert.Throws<Exception>(() => _shopService.OrderAndSellItem(orderDto));
        }
    }
}
