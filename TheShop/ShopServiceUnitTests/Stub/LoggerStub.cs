﻿using System;
using TheShop.Common.Logger;

namespace TheShop.ServiceUnitTests.Stub
{
    public class LoggerStub : ILogger
    {
        public void Info(string message)
        {
            Console.WriteLine($"{DateTime.Now} Info: {message}");
        }

        public void Error(string message)
        {
            Console.WriteLine($"{DateTime.Now} Error: {message}");
        }

        public void Debug(string message)
        {
            Console.WriteLine($"{DateTime.Now} Debug: {message}");
        }
    }
}
