﻿using Moq;
using System.Collections.Generic;
using TheShop.Common.Logger;
using TheShop.RepositoryContracts.Contracts;
using TheShop.ServiceUnitTests.Fakes;
using TheShop.ServiceUnitTests.Stub;
using TheShop.ShopServices.Services;
using TheShop.SupplierContracts.Contracts;

namespace TheShop.ServiceUnitTests
{
    public class ShopServiceFixture
    {
        public ShopService ShopService { get; set; }

        public Mock<IPurchasedItemRepository> PurchasedItemRepositoryMock = new Mock<IPurchasedItemRepository>();
        public ISupplierService supplier1Fake = new Supplier1Fake();
        public ISupplierService supplier2Fake = new Supplier2Fake();
        public ILogger loggerStub = new LoggerStub();

        public ShopServiceFixture()
        {
            ICollection<ISupplierService> suppliers = new List<ISupplierService>() { supplier1Fake, supplier2Fake };
            ShopService = new ShopService(loggerStub, PurchasedItemRepositoryMock.Object, suppliers);
        }
    }
}
