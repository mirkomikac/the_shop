﻿namespace Data.Validator
{
    public interface IValidator
    {
        void Validate();
    }
}
