﻿using Data.Models;
using System;

namespace Data.Validator
{
    public class PurchasedItemValidator : IValidator
    {
        private PurchasedItem _purchasedItem;

        public PurchasedItemValidator(PurchasedItem purchasedItem)
        {
            _purchasedItem = purchasedItem ?? throw new ArgumentNullException(nameof(purchasedItem));
        }

        public void Validate()
        {
            if (_purchasedItem.ItemGuid == Guid.Empty)
            {
                throw new ArgumentOutOfRangeException(nameof(_purchasedItem.ItemGuid));
            }

            if (_purchasedItem.BuyerUserId <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(_purchasedItem.BuyerUserId));
            }

            if (_purchasedItem.Price <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(_purchasedItem.Price));
            }

            if (_purchasedItem.PurchaseDate == null)
            {
                throw new ArgumentOutOfRangeException(nameof(_purchasedItem.PurchaseDate));
            }
        }
    }
}
