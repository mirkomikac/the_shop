﻿using System;

namespace Data.Models
{
    public class PurchasedItem
    {
        public int Id { get; set; }
        public Guid ItemGuid { get; set; }
        public int BuyerUserId { get; set; }
        public float Price { get; set; }
        public DateTime PurchaseDate { get; set; }
    }
}
