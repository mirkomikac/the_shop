﻿using TheShop.ServiceContracts.Contracts;
using TheShop.ServiceContracts.Dto;
using System;

namespace TheShop
{
    public class SimpleShopClient
    {
        private IShopService ShopService { get; set; }
        public SimpleShopClient(IShopService shopService)
        {
            ShopService = shopService ?? throw new ArgumentNullException(nameof(shopService));
        }

        public void TryOrderingAnItem(Guid itemGuid, int buyerId, int maxExpectedPrice)
        {
            try
            {
                ShopService.OrderAndSellItem(new OrderDto { ItemId = itemGuid, BuyerId = buyerId, MaxExpectedPrice = maxExpectedPrice });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public void TryPrintingAPurchasedItem(int id)
        {
            try
            {
                var item = ShopService.GetById(id);
                Console.WriteLine($"Found an item with an Id: {item.Id}");
                Console.WriteLine($"Id: {item.Id} | Price: {item.Price}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Item not found: {ex.Message}");
            }
        }
    }
}
