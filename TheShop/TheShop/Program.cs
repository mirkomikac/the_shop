﻿using System;
using TheShop.BasicShopServiceFactory;
using TheShop.ServiceContracts.Contracts;

namespace TheShop
{
    internal class Program
    {
        private static void Main(string[] _)
        {
            IFactoryMethod<IShopService> basicShopFactory = new BasicShopFactory();
            IShopService shopService = basicShopFactory.Create();

            SimpleShopClient client = new SimpleShopClient(shopService);

            client.TryOrderingAnItem(Guid.Parse("9cd9c036-db33-41a8-8b44-f6401b6aded9"), 20, 500);
            client.TryPrintingAPurchasedItem(1);
            client.TryPrintingAPurchasedItem(12);

            Console.ReadKey();
        }
    }
}