﻿using Data.Models;
using Data.Validator;
using System;
using System.Collections.Generic;
using System.Linq;
using TheShop.RepositoryContracts.Contracts;

namespace TheShop.InMemoryRepository
{
    public class PurchasedItemRepository : IPurchasedItemRepository
    {
        private IList<PurchasedItem> purchasedItems;
        private static int GENERATED_ID = 0;
        private static object syncLock = new object();

        public PurchasedItemRepository(IList<PurchasedItem> items)
        {
            purchasedItems = items ?? throw new ArgumentNullException(nameof(items));
        }

        public PurchasedItem GetById(int id)
        {
            return purchasedItems.Single(x => x.Id == id);
        }

        public void Save(PurchasedItem purchasedItem)
        {
            var purchasedItemValidator = new PurchasedItemValidator(purchasedItem);
            purchasedItemValidator.Validate();

            purchasedItem.Id = GetNextId();
            purchasedItems.Add(purchasedItem);
        }

        private int GetNextId()
        {
            lock (syncLock)
            {
                GENERATED_ID += 1;
            }

            return GENERATED_ID;
        }
    }
}
