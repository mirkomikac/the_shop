﻿using System;
using TheShop.SupplierContracts.Contracts;
using TheShop.SupplierContracts.SupplierContracts;

namespace TheShop.SupplierServices
{
    public class FastSupplierService : ISupplierService
    {
        public bool ItemInInventory(Guid guid)
        {
            if (guid == Guid.Parse("af46d6e8-d7bd-40d6-89b5-7f9c18428dcd"))
            {
                return true;
            }

            return false;
        }

        public ItemDto GetItem(Guid guid)
        {
            return new ItemDto()
            {
                Id = Guid.Parse("af46d6e8-d7bd-40d6-89b5-7f9c18428dcd"),
                Name = "ItemDto from fast supplier",
                Price = 460
            };
        }
    }
}
