﻿using System;
using TheShop.SupplierContracts.Contracts;
using TheShop.SupplierContracts.SupplierContracts;

namespace TheShop.SupplierServices
{
    public class DeltaSupplierService : ISupplierService
    {
        public bool ItemInInventory(Guid guid)
        {
            if (guid == Guid.Parse("2fdb8669-1ccf-4da9-8f15-9641afe13f3e"))
            {
                return true;
            }

            return false;
        }

        public ItemDto GetItem(Guid guid)
        {
            return new ItemDto()
            {
                Id = Guid.Parse("2fdb8669-1ccf-4da9-8f15-9641afe13f3e"),
                Name = "ItemDto from Delta supplier",
                Price = 459
            };
        }
    }
}
