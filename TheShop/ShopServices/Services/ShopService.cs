﻿using Data.Models;
using System;
using System.Collections.Generic;
using TheShop.Common.Logger;
using TheShop.RepositoryContracts.Contracts;
using TheShop.ServiceContracts.Contracts;
using TheShop.ServiceContracts.Dto;
using TheShop.SupplierContracts.Contracts;
using TheShop.SupplierContracts.SupplierContracts;

namespace TheShop.ShopServices.Services
{
    public class ShopService : IShopService
    {
        private ILogger Logger { get; set; }
        private IPurchasedItemRepository PurchasedItemRepository { get; set; }
        private ICollection<ISupplierService> Suppliers { get; set; }


        public ShopService(ILogger logger, IPurchasedItemRepository itemRepository, ICollection<ISupplierService> suppliers)
        {
            Logger = logger ?? throw new ArgumentNullException(nameof(logger));
            PurchasedItemRepository = itemRepository ?? throw new ArgumentNullException(nameof(itemRepository));
            Suppliers = suppliers ?? throw new ArgumentException(nameof(itemRepository));
        }

        public void OrderAndSellItem(OrderDto orderDto)
        {
            if(orderDto == null) {
                throw new ArgumentNullException(nameof(orderDto));
            } 

            var matchingItem = FindAnItemWithinSuppliers(orderDto);
            var noMatchingItem = matchingItem == null;
            if (noMatchingItem)
            {
                throw new Exception("Could not order item");
            }

            AddToPurchasedItems(orderDto, matchingItem);
        }

        private ItemDto FindAnItemWithinSuppliers(OrderDto orderDto)
        {
            ItemDto itemToOrder = null;
            foreach (var supplier in Suppliers)
            {
                ItemDto item = supplier.GetItem(orderDto.ItemId);

                var noSuchItem = item == null;
                if (noSuchItem)
                {
                    continue;
                }

                var isWithingTheExpectedPrice = item.Price < orderDto.MaxExpectedPrice;
                if (!isWithingTheExpectedPrice)
                {
                    continue;
                }

                itemToOrder = item;
                break;
            }

            return itemToOrder;
        }

        private void AddToPurchasedItems(OrderDto orderDto, ItemDto item)
        {
            Logger.Debug($"Trying to update ItemDto status as sold with the Id: {orderDto.ItemId}, BuyerId: {orderDto.BuyerId}");

            try
            {

                PurchasedItem purchasedItem = new PurchasedItem()
                {
                    ItemGuid = item.Id,
                    BuyerUserId = orderDto.BuyerId,
                    Price = item.Price,
                    PurchaseDate = DateTime.Now
                };

                PurchasedItemRepository.Save(purchasedItem);
                Logger.Info($"ItemDto with an Id: {orderDto.ItemId} is sold.");
            }
            catch (Exception ex)
            {
                Logger.Error($"Could not mark the item with Id: {orderDto.ItemId} as sold, for buyer {orderDto.BuyerId}");
                throw new Exception($"Could not mark the item with Id: {orderDto.ItemId} as sold, for buyer {orderDto.BuyerId} with message: {ex.Message}");
            }
        }
        public PurchasedItem GetById(int id)
        {
            return PurchasedItemRepository.GetById(id);
        }
    }
}
