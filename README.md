### The Shop

### ADRs and Considerations
- Project restructuring in order to have a better overview of the initial implementation
- Restructuring and creating proper interfaces in order to make a foundation for a testable environment and abide by DIP
- Extracted domain entities and extracted external services that could be used by others (ie Logger)
- Globar renaming 'Article' -> 'Item' since it felt more suitable
- Namespaces updates and additional restructuring
- Logger as singleton as it is natural to be a one shared instance
- Remodeling - differentiating between an item within a supplier's inventory and an actual item
- Added unit tests for core logic

- Since a supplier could have been any external service, felt natural to leave it as is (like a stub) with slight modification in terms of item retrieval and renaming to make it more percieving as an integration implementation for any service
- Currently the only necessary condition is to have a price lower than the one buyer can affor, it didn't make sense to actually expand on it, however the implementation process should be improved in a way such that the actual selection process of the supplier is somewhat random (random.ord, nist oracle) or via a seleciton algorithm such as round robin
- Also if the actual process of selection should require to find the lowest price possible, usage of tasks should be considered when checking with suppliers as it will remove the sync waiting
- Aditional considerations would be naming, better structrue, better tests and overal complexity reduction in certain parts
- Adding configuration files regarding static analysis tools, styling (stylecop) etc.
- Should refer to .NET Foundation on GitHub for best practices